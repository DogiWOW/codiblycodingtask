import React from 'react'
import Product from './Product';
import '../StyleSheets/Products.css';

function Products(props:any) {
  return (
      <>
    {
    props.filterItems.length > 0 ? 
        props.filterItems.map((item:object, i:number) => (
        <Product key={i} {...item}/>
        )):
        props.items.data?.slice(props.sliceStart, props.sliceEnd).map((item:object, i:number) => (
        <Product key={i} {...item}/>
        ))
    }
      </>
  )
}

export default Products