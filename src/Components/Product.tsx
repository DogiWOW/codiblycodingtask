import React from 'react'

function Product(props:any) {
  return (
        <tr key={props.id} style={{backgroundColor: props.color}}>
            <td>{props.id}</td>
            <td>{props.name}</td>
            <td>{props.year}</td>
        </tr>
  )
}

export default Product