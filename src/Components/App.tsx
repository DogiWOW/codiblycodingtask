import React, { Component } from 'react'
import InputIDFilter from './InputIDFilter';
import Products from './Products';
import '../StyleSheets/App.css';

export class App extends Component<any, any> {
  constructor(props:any){
    super(props);
    this.state = {
      items: [],
      DataLoaded: false,
      filterItems: [],
      sliceStart: 0,
      sliceEnd: 5
    }
  }
  componentDidMount(){
    fetch("https://reqres.in/api/products")
      .then((response) => response.json())
      .then((json) => {
          this.setState({
                items: json,
                DataLoaded: true
          });
      })
  }

  filterByID = (id:number) => {
    let newItems = this.state.items.data.filter((data:any) => {return data.id === id});
    this.setState({
      filterItems: newItems,
    });
    if(newItems.length === 0 && !isNaN(id) && id !== undefined){
      alert("No product with ID "+id+" found in database")
    }
  }

  nextPage = (prevState:any) => {
    if(prevState.items.page <= this.state.items.total_pages - 1 && this.state.filterItems.length === 0){
    this.setState({
      items: {...prevState.items, page: prevState.items.page + 1},
      sliceStart: prevState.sliceStart + 5,
      sliceEnd: prevState.sliceEnd + 5
    })
    }
  }
  prevPage = (prevState:any) => {
    if(this.state.sliceStart > 0 && this.state.filterItems.length === 0){
      this.setState({
        items: {...prevState.items, page: prevState.items.page - 1},
        sliceStart: prevState.sliceStart - 5,
        sliceEnd: prevState.sliceEnd - 5
      })
    }
  }

  render() {
    if (!this.state.DataLoaded) return <div><h1>Please be patient, data is loading...</h1></div>;

    return (
      <div id='main'>
        <div id='products_table_div'>
        <div id='filter_container'>
        <InputIDFilter filterByID={this.filterByID}  filterItems={this.state.filterItems}/>
        </div>
          <table id='products_table'>
            <tbody>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Year</th>
              </tr>
              <Products 
                    items={this.state.items}
                    filterItems={this.state.filterItems}
                    sliceStart={this.state.sliceStart}
                    sliceEnd={this.state.sliceEnd}
              />
            </tbody>
          </table>
        </div>
        <div id='product_buttons_container'>
          <button className='products_button' onClick={() => this.prevPage(this.state)}>&#8592; Previous </button>
          <button className='products_button' onClick={() => this.nextPage(this.state)}>Next &#8594;</button>
        </div>
      </div>
    )
  }
}

export default App