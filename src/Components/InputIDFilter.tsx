import React, { useState } from 'react'
import '../StyleSheets/inputIDFilter.css';

function InputIDFilter(props:any) {
  const [ searchID, setSearchID ] = useState<any>(undefined);
  return (
    <>
        <h1>Enter ID:</h1>
        <div id='number_input'>
          <input type="number" placeholder='Enter a number...' min={1} onChange={event => {setSearchID(parseInt(event.target.value))}} />
          <button className='filter_button' onClick={() => props.filterByID(searchID)}> Search </button>
          <button className='filter_button' onClick={() => props.filterByID(undefined)}> Clear </button>
        </div>
    </>
  )
}

export default InputIDFilter